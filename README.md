# Upload videos from a conference to Youtube

This utility takes a Pentabarf XML schedule file, looks for video files that match each listed talk and uploads them to a Youtube channel.

## Installation

This needs to be improved, for now here is what *I* do as the developer.

I start in a clone of this repository.

I use `pew` to handle my Python virtual environments.
From the repository directory I create a *Python 3 env* with `pew new -p python3 -r requirements.txt -a . conference-to-youtube`.
Next time I want to work on the project, I can just `pew workon conference-to-youtube`.

From that virtual environment, I install in developer mode with `pip install -e .`.

## Usage

The tool expects video files **in the current directory** named after the `slug` of the corresponding talk in the schedule file, e.g. `29-flatpak_status_update_and_future_plans.mp4`.
If you used the above installation method, you can then `pew in conference-to-youtube conference-to-youtube schedule.xml` (the first `conference-to-youtube` is the name of your virtual environment, the second one is the name of the application).

## License

This tool is released under GPL version 3 or later.
