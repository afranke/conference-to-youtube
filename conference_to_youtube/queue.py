#!/usr/bin/env python3

import datetime
import os
import re
import sys
import bs4 as BeautifulSoup


def prepare_metadata(event, conference_title):
    metadata={}
    if event.persons.person:
        talk_speakers=[]
        for person in event.persons.find_all('person'):
            talk_speakers.append(person.string)
        talk_speakers=", ".join(talk_speakers)
    else:
        talk_speakers=None
    talk_title=event.title.string if event.title else ""
    metadata['video_title']=" - ".join(filter(None,
        [conference_title, talk_speakers, talk_title]))[:100]
    talk_description=event.description.string if event.description and event.description.string else ""
    talk_description=re.sub('<br\s*?>', '\n', talk_description)
    talk_description=re.sub('<.*?>', '', talk_description)
    talk_licence=event.recording.license.string
    metadata['video_description']="\n\n".join(filter(None,
        [talk_description, talk_licence]))
    # Youtube only accepts ISO 8601 dates *in Z timezone*
    # so we strip the timezone and add a Z.
    # Terrible hack, I know, but not worth fixing
    # (especially since datetime.strptime is unable to parse ISO 8601 offsets
    # before 3.7)
    metadata['video_recording_date']=event.date.string[:-6]+'.0Z'
    # FIXME we hardcode category 28 (science and tech) and English
    metadata['video_category']="28"
    metadata['video_language']="en"
    return metadata

def queue(filepath):
    with open(filepath) as schedule:
        soup = BeautifulSoup.BeautifulSoup(schedule, 'lxml-xml')
    conference_title=soup.conference.title.string
    for event in soup.find_all('event'):
        if event.recording.optout.string == 'false':
            videofile="{}.mp4".format(event.slug.string)
            if os.path.isfile(videofile):
                metadata=prepare_metadata(event, conference_title)
                yield (videofile, metadata)

if __name__ == '__main__':
    for video in queue(sys.argv[1]):
        print(video)

