#!/usr/bin/env python3

import os

import google.oauth2.credentials

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow

# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret.
CLIENT_SECRETS_FILE = "client_secret.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPE = 'https://www.googleapis.com/auth/youtube.upload'
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

MISSING_CLIENT_SECRETS_MESSAGE = "client_secret.json missing"


def get_authenticated_service():
    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPE)
    credentials = flow.run_console()
    return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)

def upload(video, service):
    body=dict(
            snippet=dict(
                title=video[1]['video_title'],
                description=video[1]['video_description'],
                categoryId=video[1]['video_category'],
                defaultAudioLanguage=video[1]['video_language']
                ),
            status=dict(
                privacyStatus='public',
                license='creativeCommon'
                ),
            recordingDetails=dict(
                recordingDate=video[1]['video_recording_date']
                )
            )

    insert_request = service.videos().insert(
            notifySubscribers=False,
            part=','.join(body.keys()),
            body=body,
            media_body=MediaFileUpload(video[0], chunksize=-1, resumable=True)
            )

    print("Uploading “{}” ({})".format(video[1]['video_title'], video[0]))
    insert_request.next_chunk()
