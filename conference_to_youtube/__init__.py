#!/usr/bin/env python3

import sys
from . import youtube
from . import queue

def main():
    service = youtube.get_authenticated_service()
    for video in queue.queue(sys.argv[1]):
        youtube.upload(video, service)
