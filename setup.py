import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.md')) as f:
    CHANGES = f.read()
with open(os.path.join(here, 'requirements.txt')) as f:
    REQUIRES = []
    for line in f:
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        REQUIRES.append(line)
with open(os.path.join(here, 'requirements-dev.txt')) as f:
    DEV_REQUIRES = []
    for line in f:
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        DEV_REQUIRES.append(line)



setup(
    name="conference-to-youtube",
    version="0.1.0",
    description="Upload conference videos to Youtube",
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        ('License :: OSI Approved :: GNU General Public License v3 or later '
         '(GPLv3+)'),
        'Operating System :: POSIX :: Linux',
        'Environment :: Console',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP',
        ],
    author="Alexandre Franke",
    author_email="afranke@gnome.org",
    url="https://gitlab.gnome.org/afranke/conference-to-youtube",
    packages=find_packages(),
    #include_package_data=True,
    data_files=[("share/conference-to-youtube", ['client_secret.json'])],
    zip_safe=False,
    test_suite='conference-to-youtube',
    install_requires=REQUIRES,
    tests_require=DEV_REQUIRES,
    entry_points={
        'console_scripts': [
            'conference-to-youtube=conference_to_youtube:main',
            ],
        },
    )
